// Mảng
var mang = [];
var thongBaoMang = document.getElementById("thongbao-mang");

function themSo(){
    var soDuocThem = document.getElementById("them-so").value *1;
    document.getElementById("them-so").value ="";
    
    mang.push(soDuocThem);
    thongBaoMang.innerHTML = `👉 Array hiện tại: ${mang}`;
}

// Các chức năng bên dưới
// Chức năng: Tổng số dương
function funcOpen1(){
    document.body.classList.toggle("active-func1");
}

function tinhTong(){
    var thongBaoTinhTong = document.getElementById("thongbao-tinhtong");
    var tinhTong = 0;

    for (let index = 0; index < mang.length; index++) {
        if(mang[index] > 0){
            tinhTong += mang[index];
        };
    };

    thongBaoTinhTong.innerHTML = `👉 Tổng số dương: ${tinhTong}`;
}


// Chức năng: Đếm số dương
function funcOpen2(){
    document.body.classList.toggle("active-func2");
}

function demSoDuong() {
    var thongBaoDemSoDuong = document.getElementById("thongbao-demsoduong");
    var demSoDuong = 0;

    for (let index = 0; index < mang.length; index++) {
        if(mang[index] > 0){
            demSoDuong++;
        };
    };

    thongBaoDemSoDuong.innerHTML = `👉 Số dương: ${demSoDuong}`;
}

// Chức năng: Tìm số nhỏ nhất
function funcOpen3(){
    document.body.classList.toggle("active-func3");
}

function timSoNhoNhat() {
    var thongBaoSoNhoNhat = document.getElementById("thongbao-sonhonhat");
    var soNhoNhat = 0;

    for (let index = 0; index < mang.length; index++) {
        if(mang[index] < soNhoNhat){
            soNhoNhat = mang[index];
        };
    };

    thongBaoSoNhoNhat.innerHTML = `👉 Số nhỏ nhất: ${soNhoNhat}`;
}

// Chức năng: Tìm số dương nhỏ nhất
function funcOpen4(){
    document.body.classList.toggle("active-func4");
}

function timSoDuongNhoNhat() {
    var thongBaoSoDuongNhoNhat = document.getElementById("thongbao-soduongnhonhat");
    var soDuongNhoNhat = 1;
    var dem1 = 0;

    for (let index = 0; index < mang.length; index++) {
        if(mang[index]>0){
            soDuongNhoNhat = mang[index];
        }   
    }

    for (let index = 0; index < mang.length; index++) {
        
        if(mang[index] < soDuongNhoNhat && mang[index]>0){
            soDuongNhoNhat = mang[index];
        };

        if(mang[index] == 1){
            dem1 ++;
        }
    };

    if(dem1 == 0 && soDuongNhoNhat == 1){
        thongBaoSoDuongNhoNhat.innerHTML = `👉 Không có số dương nhỏ nhất!`;
    } else{
        thongBaoSoDuongNhoNhat.innerHTML = `👉 Số dương nhỏ nhất: ${soDuongNhoNhat}`;
    }
}

// Chức năng: Tìm số chẵn cuối cùng
function funcOpen5(){
    document.body.classList.toggle("active-func5");
}

function timSoChanCuoiCung() {
    var thongBaoSoChanCuoiCung = document.getElementById("thongbao-sochancuoicung");
    var soChanCuoiCung = 1;

    for (let index = 0; index < mang.length; index++) {
        if(mang[index] % 2 == 0){
            soChanCuoiCung = mang[index];
        };
    }

    if(soChanCuoiCung == 1){
        thongBaoSoChanCuoiCung.innerHTML = `👉 -1 (Không có số chẵn cuối cùng!)`;
    } else{
        thongBaoSoChanCuoiCung.innerHTML = `👉 Số chẵn cuối cùng: ${soChanCuoiCung}`;
    }
}

// Chức năng: Đổi chỗ
function funcOpen6(){
    document.body.classList.toggle("active-func6");
}

function doiCho() {
    var thongBaoDoiCho = document.getElementById("thongbao-doicho");
    var vitri1 = document.getElementById("doicho-1").value *1;
    var vitri2 = document.getElementById("doicho-2").value *1;

    if(vitri1 >= mang.length || vitri2 >= mang.length || vitri1 < 0 || vitri2 <0){
        thongBaoDoiCho.innerHTML = `👉 Vui lòng nhập vị trí hợp lệ!`;
    } else{
        var trunggian = mang[vitri1];
        mang[vitri1] = mang[vitri2];
        mang[vitri2] = trunggian;
        thongBaoDoiCho.innerHTML = `👉 Mảng sau khi đổi: ${mang}`;
    };
}

// Chức năng: Sắp xếp tăng dần
function funcOpen7(){
    document.body.classList.toggle("active-func7");
}

function compareNumbers(a, b) {
    return a - b;
  }

function sapXepTangDan() {
    var thongBaoTangDan = document.getElementById("thongbao-sapxeptangdan");

    mang.sort(compareNumbers);

    thongBaoTangDan.innerHTML = `👉 Mảng sau khi sắp xếp: ${mang}`;
}

// Chức năng: Tìm số nguyên tố đầu tiên
function funcOpen8(){
    document.body.classList.toggle("active-func8");
}

function timSoNguyenToDauTien() {
    var thongBaoSoNguyenToDauTien = document.getElementById("thongbao-songuyentodautien");
    var trunggian = 0;

    for (let index = 0; index < mang.length; index++) {
        if(mang[index] == 2 || mang[index] == 3){
            trunggian = mang[index];
            break;
        } else if(mang[index] > 1 && mang[index] % 2 != 0 && mang[index] % 3 !=0){
            trunggian = mang[index];
            break;
        }
    }

    if(trunggian == 0){
        thongBaoSoNguyenToDauTien.innerHTML = `👉 -1 (Không có số nguyên tố!)`;
    } else{
        thongBaoSoNguyenToDauTien.innerHTML = `👉 Số nguyên tố đầu tiên: ${trunggian}`;
    }
};

// Chức năng: Đếm số nguyên
function funcOpen9(){
    document.body.classList.toggle("active-func9");
}

var mang2 = [];
var thongBaoMang2 = document.getElementById("thongbao-mang2");

function themSo2(){
    var soDuocThem = document.getElementById("them-so2").value *1;
    document.getElementById("them-so2").value ="";
    
    mang2.push(soDuocThem);
    thongBaoMang2.innerHTML = `👉 Array hiện tại: ${mang2}`;
}

function demSoNguyen() {
    var thongBaoSoNguyen = document.getElementById("thongbao-demsonguyen");
    var dem = 0;

    for (let index = 0; index < mang2.length; index++) {
        if(Number.isInteger(mang2[index])){
            dem++;
        }
    };

    if(dem == 0){
        thongBaoSoNguyen.innerHTML = `👉 Không có số nguyên`;
    } else{
        thongBaoSoNguyen.innerHTML = `👉 Số nguyên: ${dem}`;
    };
};

// Chức năng: So sánh số lượng số âm và dương
function funcOpen10(){
    document.body.classList.toggle("active-func10");
}

function soSanh() {
    var thongBaoSoSanh = document.getElementById("thongbao-sosanh");
    var demSoAm = 0;
    var demSoDuong = 0;

    for (let index = 0; index < mang.length; index++) {
        if(mang[index]>0){
            demSoDuong++;
        };
        if(mang[index]<0){
            demSoAm++;
        };

    };

    if(demSoAm == demSoDuong){
        thongBaoSoSanh.innerHTML = `👉 Số âm = Số dương`;
    } else if(demSoAm > demSoDuong){
        thongBaoSoSanh.innerHTML = `👉 Số âm > Số dương`;
    } else{
        thongBaoSoSanh.innerHTML = `👉 Số dương > Số âm`;
    }
};